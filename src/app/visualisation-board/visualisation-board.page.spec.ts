import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisualisationBoardPage } from './visualisation-board.page';

describe('VisualisationBoardPage', () => {
  let component: VisualisationBoardPage;
  let fixture: ComponentFixture<VisualisationBoardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualisationBoardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisualisationBoardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
