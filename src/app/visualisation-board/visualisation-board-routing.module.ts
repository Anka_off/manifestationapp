import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualisationBoardPage } from './visualisation-board.page';
import {AddPicComponent} from './add-pic/add-pic.component';

const routes: Routes = [
  {
    path: '',
    component: VisualisationBoardPage
  },
  {
    path: 'create',
    component: AddPicComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualisationBoardPageRoutingModule {}
