import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { VisualisationBoardPageRoutingModule } from './visualisation-board-routing.module';

import { VisualisationBoardPage } from './visualisation-board.page';
import { AddPicComponent } from './add-pic/add-pic.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VisualisationBoardPageRoutingModule,
    HttpClientModule,
  ],
  declarations: [VisualisationBoardPage, AddPicComponent]
})
export class VisualisationBoardPageModule {}
